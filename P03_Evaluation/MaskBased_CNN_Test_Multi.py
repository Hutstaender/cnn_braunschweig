#####################################################################################
import glob
import os
import shelve
import time
from pathlib import Path  # only working with python >= 3.5

import numpy as np
import tensorflow.keras.backend as backend
from numba import prange
from tensorflow.keras.models import load_model

import settings

###################################################
# 1 Settings
###################################################
TEST_DATA_PATH = settings.TEST_DATA_PATH
RESULTS_PATH = settings.RESULTS_PATH
FRAM_LENGTH = settings.FRAM_LENGTH  # 260  # k_in: The number of input and output frequency bins
NUMBER_OF_TESTS = settings.NUMBER_OF_TRAININGS

LOOK_BACKWARD = 2
LOOK_FORWARD = 2
INPUT_SHAPE = (FRAM_LENGTH, (LOOK_BACKWARD + 1 + LOOK_FORWARD))
INPUT_SHAPE2 = (FRAM_LENGTH, 1)


# Function used for reshape input data with context frames
def reshapeDataMatrix(data_matrix, look_backward=1, look_forward=2):
    start = time.time()
    new_dim_len = look_backward + look_forward + 1
    data_matrix_out = np.zeros((data_matrix.shape[0], data_matrix.shape[1], new_dim_len))
    for i in range(0, data_matrix.shape[0]):
        for j in range(-look_backward, look_forward + 1):
            if i < look_backward:
                idx = max(i + j, 0)
            elif i >= data_matrix.shape[0] - look_forward:
                idx = min(i + j, data_matrix.shape[0] - 1)
            else:
                idx = i + j
            data_matrix_out[i, :, j + look_backward] = data_matrix[idx, :]

    print('> Reshaping time: ' + str(time.time() - start))
    return data_matrix_out


###################################################
# 2 Main Process
###################################################
# @njit(parallel=True)
def test_main():
    print('RUNNING: ' + __file__)
    startAll = time.time()  # Timestamp for measuring execution time

    # pathlist = [p for p in glob.glob(os.path.join(RESULTS_PATH ,'training_results_1') + "**/*.h5", recursive=True)]
    # print(pathlist)

    for i in range(0, NUMBER_OF_TESTS):
        np.random.seed(1337)  # for reproducibility

        ###################################################
        # 2 Load data
        ###################################################
        print('> Loading data... ')
        # Load input noisy data
        print('> Loading test noisy input... ')
        Test_Noisy = shelve.open(os.path.join(TEST_DATA_PATH, 'test_noisy.slv'))
        test_noisy = Test_Noisy['spectrogram_test_noisy']
        test_noisy = reshapeDataMatrix(test_noisy, look_backward=LOOK_BACKWARD, look_forward=LOOK_FORWARD)
        print('     Input noisy data shape: %s' % str(test_noisy.shape))
        Test_Noisy.close()
        # load noise component
        print('> Loading test noise component... ')
        input_noise = shelve.open(os.path.join(TEST_DATA_PATH, 'test_pure_noisy.slv'))
        test_noise_component = input_noise['spectrogram_test_pure_noise']
        input_noise.close()
        # load speech component
        print('> Loading test speech component... ')
        input_speech = shelve.open((os.path.join(TEST_DATA_PATH, "test_clean.slv")))
        test_speech_component = input_speech['spectrogram_test_clean']
        input_speech.close()

        # load unnormalized noisy speech component for estimate the enhanced speech
        print('> Loading test speech component... ')

        input_noisy_unnorm = shelve.open((os.path.join(TEST_DATA_PATH, "test_noisy_unnorm.slv")))
        test_noisy_speech = input_noisy_unnorm['spectrogram_test_noisy_unnorm']
        input_noisy_unnorm.close()

        ###################################################
        # 2.1 Data reshaping
        ###################################################
        test_noise_component = np.reshape(test_noise_component,
                                          (test_noise_component.shape[0], test_noise_component.shape[1], 1))
        print('     test_noise_component data shape: %s' % str(test_noise_component.shape))

        test_speech_component = np.reshape(test_speech_component,
                                           (test_speech_component.shape[0], test_speech_component.shape[1], 1))
        print('     test_speech_component data shape: %s' % str(test_speech_component.shape))

        test_noisy_speech = np.reshape(test_noisy_speech,
                                       (test_noisy_speech.shape[0], test_noisy_speech.shape[1], 1))
        print('     test_noisy_speech data shape: %s' % str(test_noisy_speech.shape))

        # check for multiple history files
        history_files = [p for p in
                         glob.glob(os.path.join(RESULTS_PATH, 'training_results_' + str(i + 1)) + "**/*.h5",
                                   recursive=True)]

        # loop over every best generated model
        for j in prange(0, len(history_files)):
            #####################################################################################
            # 3 define model
            #####################################################################################
            # model_path = RESULTS_PATH + '/training_results_' + str(i + 1) + '\mask_CNN_3CL.h5'
            model_path = os.path.join(RESULTS_PATH, 'training_results_' + str(i + 1),
                                      os.path.basename(history_files[j]))

            model = load_model(model_path, compile=False)

            #####################################################################################
            # 5 Prediction
            # 5.1 filtered speech and noise prediction (s_tilde and n_tilde)
            #####################################################################################
            [n_tilde, useless, s_tilde] = model.predict([test_noisy, test_noise_component, test_speech_component])
            print('     n_tilde shape: %s' % str(n_tilde.shape))
            print('     s_tilde shape: %s' % str(s_tilde.shape))

            # check if path exists, if not create it
            results_dir = RESULTS_PATH + '/test_results_' + str(i + 1) + '/' + os.path.basename(history_files[j])[:-3]
            Path(results_dir).mkdir(parents=True, exist_ok=True)

            # recon_n_tilde = results_dir + '/test_n_tilde_3CL.mat'
            # recon_n_tilde = os.path.normcase(recon_n_tilde)
            # sio.savemat(recon_n_tilde, {'test_n_tilde': n_tilde})
            s_hat_slv = shelve.open((os.path.join(results_dir, "test_n_tilde_3CL.slv")))
            s_hat_slv['test_n_tilde'] = n_tilde
            s_hat_slv.close()

            # recon_s_tilde = results_dir + '/test_s_tilde_3CL.mat'
            # recon_s_tilde = os.path.normcase(recon_s_tilde)
            # sio.savemat(recon_s_tilde, {'test_s_tilde': s_tilde})
            s_tilde_slv = shelve.open((os.path.join(results_dir, "test_s_tilde_3CL.slv")))
            s_tilde_slv['test_s_tilde'] = s_tilde
            s_tilde_slv.close()

            #####################################################################################
            # 5.2 enhanced speech prediction (s_hat)
            #####################################################################################
            [s_hat2, useless, s_hat1] = model.predict([test_noisy, test_noisy_speech,
                                                       test_noisy_speech])  # all the three outputs are the same, needed cause network was trained with 3 inputs
            print('     s_hat shape: %s' % str(s_hat2.shape))

            # recon_s_hat = results_dir + '/test_s_hat_3CL.mat'
            # recon_s_hat = os.path.normcase(recon_s_hat)
            # sio.savemat(recon_s_hat, {'test_s_hat': s_hat2})
            s_hat_slv = shelve.open((os.path.join(results_dir, "test_s_hat_3CL.slv")))
            s_hat_slv['test_s_hat'] = s_hat2
            s_hat_slv.close()

            backend.clear_session()  # resets all network parameters for next test
            print('Test 3CL Nr. ' + str(i + 1) + '/' + str(NUMBER_OF_TESTS) + ' Epoch: ' + str(
                j + 1) + '/' + str(len(history_files)) + ' finished. #######################################')

    print('End Time: ' + str(time.time() - startAll) + 's')


# this call is only for debugging. For executing the script use the main.py it calls multiple steps together
if __name__ == '__main__':
    test_main()
