import csv
import glob
import inspect  # to get a functions name dynamically
import ntpath
import os
import pathlib
import time  # for time measurement

import librosa
import numpy as np
import pandas as pd
from numba import prange  # parallel for loops
from pesq import pesq
from pystoi import stoi

import settings

###################################################
# 1 Settings
###################################################
sr = 16000
INFO_PATH = r"F:\Trainsets\Components-Loss-master\log_testset.txt"
TEST_CLEAN_PATH = settings.test_clean_path
RESULTS_PATH = settings.RESULTS_PATH

number_of_tests = settings.NUMBER_OF_TRAININGS


###################################################
# 2 Scoring
###################################################
def calculate_score(clean_path, enhanced_path, number_of_testset, number_of_epoch):
    """
    calculate_score function description

    @param clean_path: file path of the clean audio data, e.g. r'F:\Trainsets\Components-Loss-master\audio_data\Edinburgh\03_test\clean'
    @param enhanced_path: file path of the enhanced audio data, e.g. r'F:\Trainsets\Components-Loss-master_slv\training_results_16khz_28spk_2022-07-27_20validationdata_meansub_batch128\final_audio_data_1\tim_cnn_3cl_015'
    @param number_of_testset: defines the number of the current testset if multiple trainings or tests were started at once
    @param number_of_epoch: defines the number of the current epoch if multiple history files with different weights were saved during training
    @return: A numpy array with all filenames, snrlevels, pesq scores, stoi scores + a mean value for pesq + a meand value for stoi
    @raise keyError: no error handling added
    """
    start = time.time()

    pathlist1 = [p for p in glob.glob(clean_path + "**/*.wav", recursive=True)]
    pathlist2 = [p for p in glob.glob(enhanced_path + "**/*.wav", recursive=True)]
    x = pd.read_csv(INFO_PATH, sep="\t", header=None)
    file_names = []
    snr_levels = []

    for index, row in x.iterrows():
        file_names.append(row[0][0:8])
        snr_levels.append(row[0][-4:])

    pesq_scores = ['none'] * len(file_names)
    stoi_scores = ['none'] * len(file_names)
    for line, count in zip(pathlist1, pathlist2):
        clean = librosa.load(line, sr=sr)[0]
        enhanced = librosa.load(count, sr=sr)[0]
        current_file_name = ntpath.basename(line)[:-4]
        for i in prange(len(file_names)):
            if current_file_name == file_names[i]:
                pesq_scores[i] = pesq(sr, clean, enhanced, 'wb')
                stoi_scores[i] = stoi(clean, enhanced, sr)
                break

    d = {'Name': file_names, 'SNR': snr_levels, 'PESQ': pesq_scores, 'STOI': stoi_scores}
    d = pd.DataFrame(data=d)
    d.to_csv(path_or_buf=os.path.join(os.path.dirname(enhanced_path),
                                      'audiofileInformation_' + str(number_of_testset + 1) + '_Epoch' + str(
                                          number_of_epoch) + '.txt'),
             index=False)
    print(d)
    print("> " + inspect.currentframe().f_code.co_name + " execution time: " + str(time.time() - start))
    return np.c_[file_names, snr_levels, pesq_scores, stoi_scores], np.mean(pesq_scores), np.mean(stoi_scores)


def get_snr_levels(list1):
    start = time.time()
    # initialize a null list
    unique_list = []

    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x[1] not in unique_list:
            unique_list.append(x[1])

    print("> " + inspect.currentframe().f_code.co_name + " execution time: " + str(time.time() - start))
    # return list
    return unique_list


def calculate_score_for_snr(scores, snr_levels, column):
    start = time.time()
    pesq_per_snr = []
    for i in prange(len(snr_levels)):
        current_pesq_sum = 0
        current_pesq_count = 0
        for x in scores:
            if x[1] == snr_levels[i]:
                current_pesq_sum += x[column].astype(float)
                current_pesq_count += 1
        pesq_per_snr.append([snr_levels[i], current_pesq_sum / current_pesq_count])

    print("> " + inspect.currentframe().f_code.co_name + " execution time: " + str(time.time() - start))
    return pesq_per_snr


def score_main():
    print('RUNNING: ' + __file__)

    for i in prange(0, number_of_tests):
        # check for multiple model history files
        history_files = [p for p in
                         glob.glob(os.path.join(RESULTS_PATH, 'final_audio_data_' + str(i + 1), "*/"),
                                   recursive=True)]

        for j in prange(0, len(history_files)):
            print('\n####################### SCORE.PY ##############################')
            print('calculating: ' + pathlib.PurePath(history_files[j]).name + ' ...')
            enhanced_path = os.path.join(RESULTS_PATH, 'final_audio_data_' + str(i + 1),
                                         pathlib.PurePath(history_files[j]).name)

            score_list, mean_pesq, mean_stoi = calculate_score(TEST_CLEAN_PATH, enhanced_path, i, int(
                pathlib.PurePath(history_files[j]).name[-3:]))
            all_snr_levels = get_snr_levels(score_list)

            # scores per snr
            pesq_per_snr = calculate_score_for_snr(score_list, all_snr_levels, 2)  # column 2 contains all pesq values
            print('\npesqPerSnr')
            print(pesq_per_snr)
            pesq_per_snr = pd.DataFrame(data=pesq_per_snr, columns=['SNR', 'PESQ'])
            pesq_per_snr.to_csv(
                path_or_buf=os.path.join(os.path.dirname(enhanced_path),
                                         'PesqPerSnr_' + str(i + 1) + '_Epoch' + str(int(
                                             pathlib.PurePath(history_files[j]).name[-3:])) + '.txt'), index=False)

            stoi_per_snr = calculate_score_for_snr(score_list, all_snr_levels, 3)  # column 3 contains all stoi values
            print('\nstoiPerSnr')
            print(stoi_per_snr)
            stoi_per_snr = pd.DataFrame(data=stoi_per_snr, columns=['SNR', 'STOI'])
            stoi_per_snr.to_csv(
                path_or_buf=os.path.join(os.path.dirname(enhanced_path),
                                         'StoiPerSnr_' + str(i + 1) + '_Epoch' + str(int(
                                             pathlib.PurePath(history_files[j]).name[-3:])) + '.txt'), index=False)

            # mean scores
            print('\nmean PESQ:' + str(mean_pesq))
            print('mean STOI:' + str(mean_stoi))
            with open(os.path.join(os.path.dirname(enhanced_path), 'MeanScores_' + str(i + 1) + '_Epoch' + str(int(
                    pathlib.PurePath(history_files[j]).name[-3:])) + '.txt'), 'w',
                      newline='') as csvfile:
                my_csv = csv.writer(csvfile, delimiter=' ')
                my_csv.writerow(['PESQ: ' + str(mean_pesq)])
                my_csv.writerow(['STOI: ' + str(mean_stoi)])


# this call is only for debugging. For executing the script use the main.py it calls multiple steps together
if __name__ == '__main__':
    score_main()
