import os
import numpy as np
import librosa
import shelve
import pprint
import settings
import glob
import pathlib

###################################################
# 1 Settings
###################################################
Test_Data_Path = settings.TEST_DATA_PATH
RESULTS_PATH = settings.RESULTS_PATH

nfft = settings.FFT
number_of_tests = settings.NUMBER_OF_TRAININGS


###################################################
# 2 Reconstruction
###################################################
def rec_main():
    print('RUNNING: ' + __file__)

    for i in range(0, number_of_tests):

        # check for multiple history files
        history_files = [p for p in
                         glob.glob(os.path.join(RESULTS_PATH, 'test_results_' + str(i + 1), "*/"),
                                   recursive=True)]
        print(history_files)
        for j in range(0, len(history_files)):
            s_hat_slv = shelve.open((os.path.join(os.path.dirname(history_files[j]), "test_s_hat_3CL.slv")))
            s_hat = s_hat_slv['test_s_hat']
            s_hat_slv.close()
            s_hat = np.reshape(s_hat, (s_hat.shape[0], s_hat.shape[1]))

            test_noisy_slv = shelve.open((os.path.join(Test_Data_Path, "test_noisy_unnorm.slv")))
            pprint.pprint(list(test_noisy_slv.keys()))
            file_length = np.array(test_noisy_slv['file_length'])
            test_noisy_phase = test_noisy_slv['phase_test_noisy_unnorm']
            file_name = np.array(test_noisy_slv['file_names'])
            length = np.sum(file_length)
            test_noisy_slv.close()

            spectrum3 = s_hat * np.exp(1j * test_noisy_phase)
            spectrum3 = np.transpose(np.delete(spectrum3, [257, 258, 259], axis=1))

            time_based_audio_data = librosa.istft(spectrum3, window='hann', n_fft=512, hop_length=256, win_length=512,
                                                  center=True,
                                                  length=length)

            current_file_position = 0
            for k in range(len(file_length)):

                temp_data = time_based_audio_data[current_file_position:(current_file_position + file_length[k])]
                current_file_position = current_file_position + file_length[k]
                destination_2 = os.path.join(RESULTS_PATH, 'final_audio_data_' + str(i + 1),
                                             pathlib.PurePath(history_files[j]).name)
                pprint.pprint(destination_2)
                if not os.path.exists(destination_2):
                    os.makedirs(destination_2)
                import soundfile as sf

                sf.write(os.path.join(destination_2, file_name[k]), temp_data, 16000, 'PCM_24')
                print(temp_data)


# this call is only for debugging. For executing the script use the main.py it calls multiple steps together
if __name__ == '__main__':
    rec_main()
