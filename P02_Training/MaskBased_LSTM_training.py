# -*- coding: utf-8 -*-
"""
Created on Sun May  1 20:01:41 2022

@author: Tobi
This file is still wip!!! currently not for general use

"""
import numpy

"""
 Fragen von Tobias: 
    Warum ist y_label nur ein array aus 0 (train_residual)?
    müsste es nicht clean speechs ein?
    Was muss ich bei Multiply machen?
    
    Muss die mask mit input noisy oder input noise component multpl. werden?
    warum haben sie unterschiedliche Größen?
    
    Warum wird beim Noise Suppression Netz nur noisy speech rein gegeben
    und beim Speech restoration auch clean und pure
    
    Wie viele Ausgabe Neuronen?
    
    Seite 11 letzter Absatz
    C als Input und K als output
    -> C = (L_ + 1 + L+)* (K/2 + 1)
    -> K = 256  ==> C = 645
    -> was machen frame length und frame shift?
    
#o2.shape
#multiply layer selber schreiben => custom layer schreiben
#darin die mask und input noisy geben und die multiply funktion selber umsetzen
#evetnuell auch in matlab

"""
# import the necessary packages
import os
import time
import shelve
from tensorflow.keras.models import Model
import scipy.io as sio
import tensorflow.keras.backend as backend
from tensorflow.keras.layers import Input, Dense, LSTM, Reshape, Multiply
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K
import numpy as np
import h5py
from tensorflow.keras.utils import plot_model
import tensorflow.keras.callbacks as cbs
import pprint
import settings
import math
import parfor
from numba import njit, prange

# define the architecture using Keras
###################################################
# 1 Settings
###################################################
Results_Path = r'F:\Trainsets\Components-Loss-master\training_results_Tobi_LSTM_shape_test'
TRAIN_DATA_PATH = settings.TRAIN_DATA_PATH_LSTM
# RESULTS_PATH = settings.RESULTS_PATH_LSTM

fram_length = 129
LOOK_BACKWARD = 2
LOOK_FORWARD = 2
INPUT_SHAPE = ((LOOK_BACKWARD + 1 + LOOK_FORWARD), fram_length)
INPUT_SHAPE2 = (fram_length, 1)

@njit(parallel=True)
def reshapeDataMatrix(data_matrix, look_backward=1, look_forward=2):
    new_dim_len = look_backward + look_forward + 1
    data_matrix_out = np.zeros((data_matrix.shape[0], data_matrix.shape[1], new_dim_len))
    for i in prange(0, data_matrix.shape[0]):
        for j in range(-look_backward, look_forward + 1):
            if i < look_backward:
                idx = max(i + j, 0)
            elif i >= data_matrix.shape[0] - look_forward:
                idx = min(i + j, data_matrix.shape[0] - 1)
            else:
                idx = i + j
            data_matrix_out[i, :, j + look_backward] = data_matrix[idx, :]
            #               [2115,129,5]

    return data_matrix_out


@njit(parallel=True)
def reshapeDataMatrix2(data_matrix):
    sequence_size = 100
    data_matrix_out = np.zeros((int(math.ceil(data_matrix.shape[0] / sequence_size)), sequence_size, 645),
                               dtype=numpy.float64)

    for h in prange(0, data_matrix_out.shape[0]):  # 22
        for i in range(0, data_matrix_out.shape[1]):  # 100
            temp_vec = []

            if h * sequence_size + i == data_matrix.shape[0]:
                break

            for j in range(0, data_matrix.shape[1]):  # 129
                for k in range(0, data_matrix.shape[2]):  # 5
                    temp_vec.append(data_matrix[h * sequence_size + i, j, k])
            data_matrix_out[h, i, :] = numpy.array(temp_vec)
        else:
            continue
        break

    return data_matrix_out


# data_matrix = np.random.rand(3115000, 129, 5)
# start = time.time()
# test_matrix = reshapeDataMatrix2(data_matrix)
# print('execution time: ' + str(time.time() - start))
# pprint.pprint(test_matrix)


def myloss(y_true, y_pred):
    return K.mean(K.square())


###################################################
# 2 Load data
###################################################

print('> Loading data... ')

# Load input noisy data
print('  >> Loading input noisy training data... ')
Train_Noisy = shelve.open(os.path.join(TRAIN_DATA_PATH, 'train_noisy.slv'))
train_input_noisy = np.array(Train_Noisy['training_input_noisy'])  # evtl. noch transpose
print('     Input noisy training data shape: %s' % str(train_input_noisy.shape))
print('  >> Reshaping input training data... ')

start = time.time()
train_input_noisy_2 = reshapeDataMatrix(train_input_noisy, look_backward=LOOK_BACKWARD, look_forward=LOOK_FORWARD)
print('     Input noisy training _2 reshapeMatrix: %s' % str(train_input_noisy_2.shape))
print('reshaping time for train_input_noisy: ' + str(time.time() - start))

start = time.time()
train_input_noisy_2 = reshapeDataMatrix2(train_input_noisy_2)
print('     Input noisy training _2 reshapeMatrix2: %s' % str(train_input_noisy_2.shape))
"""
train_input_noisy_2 = train_input_noisy_2.reshape([-1, train_input_noisy_2.shape[2], train_input_noisy_2.shape[1]])
print('     Input noisy shape after reshape: %s' % str(train_input_noisy_2.shape))
"""
print('reshaping time for train_input_noisy_2: ' + str(time.time() - start))
Train_Noisy.close()

print('  >> Loading input noisy data for validation... ')
# Load input noisy data for validation
Valid_noisy = shelve.open(os.path.join(TRAIN_DATA_PATH, 'valid_noisy.slv'))
vali_input_noisy = np.array(Valid_noisy['validation_input_noisy'])  # evtl. noch transpose
print('  >> Reshaping input validation data... ')

vali_input_noisy_2 = reshapeDataMatrix(vali_input_noisy, look_backward=LOOK_BACKWARD, look_forward=LOOK_FORWARD)
print('     Input noisy validation data shape: %s' % str(vali_input_noisy.shape))

vali_input_noisy_2 = vali_input_noisy_2.reshape([-1, vali_input_noisy_2.shape[2], vali_input_noisy_2.shape[1]])
print('     Input noisy validation data shape: %s' % str(vali_input_noisy_2.shape))
Valid_noisy.close()

# Load clean speech training target data, which is also be used as speech component quality training input. See the first term in (6)
print('  >> Loading clean speech training data... ')
Train_Clean = shelve.open(os.path.join(TRAIN_DATA_PATH, 'train_clean.slv'))
train_clean_speech = np.array(Train_Clean['training_clean_speech'])
print('     Clean speech training data shape: %s' % str(train_clean_speech.shape))
Train_Clean.close()

# Load clean speech validation target data, which is also be used as speech component quality validation input. See the first term in (6)
print('  >> Loading clean speech validation data... ')
Valid_Clean = shelve.open(os.path.join(TRAIN_DATA_PATH, 'valid_clean.slv'))
vali_clean_speech = np.array(Valid_Clean['validation_clean_speech'])
print('     Clean speech validation data shape: %s' % str(vali_clean_speech.shape))
Valid_Clean.close()

print('> Data loading finished . Reshaping...')
"""
# Load input noisy data
print('  >> Loading input noisy training data... ')
mat_input = "./training_data/training_input_noisy.mat"
file_h5py_train_input = h5py.File(mat_input, 'r')
train_input_noisy = file_h5py_train_input.get('training_input_noisy')
train_input_noisy = np.array(train_input_noisy)
train_input_noisy = np.transpose(train_input_noisy)
print(train_input_noisy.shape)
print('  >> Reshaping input data... ')
train_input_noisy_2 = reshapeDataMatrix(train_input_noisy, look_backward=LOOK_BACKWARD, look_forward=LOOK_FORWARD)

print('     Input noisy shape after DataMatrix: %s' % str(train_input_noisy_2.shape))
train_input_noisy_2 = train_input_noisy_2.reshape([-1, train_input_noisy_2.shape[2], train_input_noisy_2.shape[1]])
print('     Input noisy shape after reshape: %s' % str(train_input_noisy_2.shape))

# train_input_noisy_2 = train_input_noisy_2.reshape(train_input_noisy_2.shape[0], 1, 645)
# print(train_input_noisy_2.shape)


# Load input noisy data for validation
print('  >> Loading input noisy data for validation... ')
mat_input_vali = "./training_data/validation_input_noisy.mat"
file_h5py_vali_input = h5py.File(mat_input_vali, 'r')
vali_input_noisy = file_h5py_vali_input.get('validation_input_noisy')
vali_input_noisy = np.array(vali_input_noisy)
vali_input_noisy = np.transpose(vali_input_noisy)
print(vali_input_noisy.shape)
print('  >> Reshaping input validation data... ')
vali_input_noisy_2 = reshapeDataMatrix(vali_input_noisy, look_backward=LOOK_BACKWARD, look_forward=LOOK_FORWARD)
vali_input_noisy_2 = vali_input_noisy_2.reshape([-1, vali_input_noisy_2.shape[2], vali_input_noisy_2.shape[1]])
print('     Input noisy validation data shape: %s' % str(vali_input_noisy_2.shape))

# Load clean speech training target data, which is also be used as speech component quality training input. See the first term in (6)
print('  >> Loading clean speech training data... ')
mat_target = "./training_data/training_clean_speech.mat"
training_target = h5py.File(mat_target, 'r')
train_clean_speech = training_target.get('training_clean_speech')
train_clean_speech = np.array(train_clean_speech)
train_clean_speech = np.transpose(train_clean_speech)
print('     Clean speech training data shape: %s' % str(train_clean_speech.shape))

# Load clean speech validation target data, which is also be used as speech component quality validation input. See the first term in (6)
print('  >> Loading clean speech validation data... ')
mat_target_vali = "./training_data/validation_clean_speech.mat"
vali_target = h5py.File(mat_target_vali, 'r')
vali_clean_speech = vali_target.get('validation_clean_speech')
vali_clean_speech = np.array(vali_clean_speech)
vali_clean_speech = np.transpose(vali_clean_speech)
print('     Clean speech validation data shape: %s' % str(vali_clean_speech.shape))
"""

###################################################
# 2.1 Data reshaping
###################################################

# vali_clean_speech = np.reshape(vali_clean_speech, (vali_clean_speech.shape[0], vali_clean_speech.shape[1], 1))
# print('     vali_clean_speech shape: %s' % str(vali_clean_speech.shape))
# train_clean_speech = np.reshape(train_clean_speech, (train_clean_speech.shape[0], train_clean_speech.shape[1], 1))
# print('     train_clean_speech shape: %s' % str(train_clean_speech.shape))

train_clean_speech = train_clean_speech[0:train_input_noisy.shape[0]]

###################################################
# 3 define model
###################################################

"""
mit input_noise_component: graph disconnected
mit input_noisy: ValueError: Shapes (None, 260, 1) and (None, 260, 5) are incompatible
mit mask: funktioniert

"""
input_noisy = Input(shape=(INPUT_SHAPE))  # INPUT_SHAPE = (fram_length,(LOOK_BACKWARD + 1 + LOOK_FORWARD))
input_noise_component = Input(shape=(INPUT_SHAPE2))  # INPUT_SHAPE2 = (fram_length,1)

# d1 = Dense(425, activation=("relu"))(input_noisy)
d1 = Dense(425, activation=("relu"))(input_noise_component)

l1 = LSTM(425, return_sequences=(True))(d1)
l2 = LSTM(425, return_sequences=(False))(l1)

d2 = Dense(425, activation=("relu"))(l2)
d3 = Dense(425, activation=("relu"))(d2)
mask = Dense(129, activation=("tanh"))(d3)

mask = Reshape([mask.shape[1], 1])(mask)

n_tilde = Multiply()([mask, input_noise_component])

model = Model(inputs=[input_noise_component], outputs=[n_tilde])
# model = Model(inputs=[input_noisy, input_noise_component], outputs=[n_tilde])
model.output_shape
# plot_model(model, to_file='model.png')

print(model.summary())

#####################################################################################
# 4 Training setting
#####################################################################################

nb_epochs = 100
batch_size = 128  # 16
learning_rate = 5e-3  # 5e-4
adam_wn = Adam(learning_rate=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

model.compile(loss="mean_squared_error", optimizer=adam_wn,
              metrics=["accuracy"])

best_weights = Results_Path + '/training_results_1/mask_CNN_3CL' + '.h5'
best_weights = os.path.normcase(best_weights)
model_save = cbs.ModelCheckpoint(best_weights, monitor='val_loss', save_best_only=True,
                                 mode='auto')

print("> Training model " + "using Batch-size: " + str(batch_size) + ", Learning_rate: " + str(learning_rate) + "...")
# H = model.fit([train_input_noisy_2, train_input_noisy],
H = model.fit([ train_input_noisy],
              train_clean_speech,
              epochs=nb_epochs,
              verbose=1,
              batch_size=batch_size,
              shuffle=True,
              initial_epoch=0,
              callbacks=[model_save],  # callbacks=[reduce_LR, stop_str, model_save],
              validation_data=([vali_input_noisy_2, vali_input_noisy],
                               vali_clean_speech))

# anstatt train residual noiser power target soll clean speech als output

start = time.time()

ValiLossVec = './training_results' + '/mask_CNN_3CL' + '_validationloss.mat'
ValiLossVec = os.path.normcase(ValiLossVec)  # directory
sio.savemat(ValiLossVec, {'Vali_loss_Vec': H.history['val_loss']})

LossVec = './training_results' + '/mask_CNN_3CL' + '_loss.mat'
LossVec = os.path.normcase(LossVec)  # directory
sio.savemat(LossVec, {'loss_Vec': H.history['loss']})

print("> Saving Completed, Time : ", time.time() - start)
print('> +++++++++++++++++++++++++++++++++++++++++++++++++++++ ')

backend.clear_session()
