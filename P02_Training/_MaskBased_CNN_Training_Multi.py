import datetime
import os
import shelve
import time
from pathlib import Path  # only working with python >= 3.5

import matplotlib.pyplot as plt
import numpy as np
import tensorflow.keras.backend as backend
import tensorflow.keras.callbacks as cbs
from numba import njit, prange
from tensorflow.keras import backend as K
from tensorflow.keras import initializers
from tensorflow.keras.layers import Conv1D, MaxPooling1D, UpSampling1D  # , AveragePooling1D
from tensorflow.keras.layers import Input, Add, Multiply, PReLU  # , Activation, Average, BatchNormalization
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam

import settings
from P04_Tools.plt_loss import plot_learning_curves

#######################################################################################################################
# 1 Preparation - Settings
#######################################################################################################################
TRAIN_DATA_PATH = settings.TRAIN_DATA_PATH
RESULTS_PATH = settings.RESULTS_PATH
FRAM_LENGTH = settings.FRAM_LENGTH  # k_in: The number of input and output frequency bins
NUMBER_OF_TRAININGS = settings.NUMBER_OF_TRAININGS

nb_epochs = 100  # 100
batch_size = 8192  # 16
learning_rate = 5e-4  # 5e-4

#######################################################################################################################
# 1.1 CNN parameters
#######################################################################################################################
n1 = 60
n2 = 120
n3 = 60
n10 = 60
N_cnn = 15
N_cnn_last = N_cnn

LOOK_BACKWARD = 2
LOOK_FORWARD = 2
INPUT_SHAPE = (FRAM_LENGTH, (LOOK_BACKWARD + 1 + LOOK_FORWARD))
INPUT_SHAPE2 = (FRAM_LENGTH, 1)


# Function used for reshape input data with context frames
@njit(parallel=True)
def reshapeDataMatrix(data_matrix, look_backward=1, look_forward=2):
    new_dim_len = look_backward + look_forward + 1
    data_matrix_out = np.zeros((data_matrix.shape[0], data_matrix.shape[1], new_dim_len))
    for i in prange(0, data_matrix.shape[0]):
        for j in range(-look_backward, look_forward + 1):
            if i < look_backward:
                idx = max(i + j, 0)
            elif i >= data_matrix.shape[0] - look_forward:
                idx = min(i + j, data_matrix.shape[0] - 1)
            else:
                idx = i + j
            data_matrix_out[i, :, j + look_backward] = data_matrix[idx, :]

    return data_matrix_out


#######################################################################################################################
# 1.2 The third component loss term in (6) wieghted by beta.
#######################################################################################################################
def powernorm(x):
    power = K.sum(x ** 2)
    power_sqr = K.sqrt(power)
    normalized = x / power_sqr  # (power_sqr+sys.float_info.epsilon)
    return normalized


def my_loss(y_true, y_pred):
    y_true_nor = powernorm(y_true)
    y_pred_nor = powernorm(y_pred)
    return K.mean(K.square(y_pred_nor - y_true_nor), axis=-1)


def train_main():
    startAll = time.time()
    for i in range(0, NUMBER_OF_TRAININGS):
        backend.clear_session()
        np.random.seed(1337)  # for reproducibility

        tic = time.time()
        print('> Loading data... ')
        # Load input noisy data
        print('  >> Loading input noisy training data... ')
        Train_Noisy = shelve.open(os.path.join(TRAIN_DATA_PATH, 'train_noisy.slv'))
        train_input_noisy = np.array(Train_Noisy['training_input_noisy'])
        print('  >> Reshaping input training data... ')
        start = time.time()
        train_input_noisy = reshapeDataMatrix(train_input_noisy, look_backward=LOOK_BACKWARD, look_forward=LOOK_FORWARD)
        print("   > reshapeDataMatrix() execution time: " + str(time.time() - start))
        print('     Input noisy training data shape: %s' % str(train_input_noisy.shape))
        Train_Noisy.close()

        print('  >> Loading input noisy data for validation... ')
        # Load input noisy data for validation
        Valid_noisy = shelve.open(os.path.join(TRAIN_DATA_PATH, 'valid_noisy.slv'))
        vali_input_noisy = np.array(Valid_noisy['validation_input_noisy'])
        print('  >> Reshaping input validation data... ')
        start = time.time()
        vali_input_noisy = reshapeDataMatrix(vali_input_noisy, look_backward=LOOK_BACKWARD, look_forward=LOOK_FORWARD)
        print("   > reshapeDataMatrix() execution time: " + str(time.time() - start))
        print('     Input noisy validation data shape: %s' % str(vali_input_noisy.shape))
        Valid_noisy.close()

        # load input data for clean noise component, which is also be used as noise component quality training target. See the third term in (6)
        print('  >> Loading clean noise training data... ')
        Train_pure_noise = shelve.open(os.path.join(TRAIN_DATA_PATH, 'train_pure_noise.slv'))
        train_pure_noise = np.array(Train_pure_noise['training_pure_noise'])
        print('     Clean noise training data shape: %s' % str(train_pure_noise.shape))
        Train_pure_noise.close()

        # Load validation data for clean noise component, which is also be used as noise component quality training target. See the third term in (6)
        print('  >> Loading clean noise validation data... ')
        Valid_pure_noise = shelve.open(os.path.join(TRAIN_DATA_PATH, 'valid_pure_noisy.slv'))
        vali_pure_noise = np.array(Valid_pure_noise['validation_pure_noise'])
        print('     Clean noise validation data shape: %s' % str(vali_pure_noise.shape))
        Valid_pure_noise.close()

        # Load clean speech training target data, which is also be used as speech component quality training input. See the first term in (6)
        print('  >> Loading clean speech training data... ')
        Train_Clean = shelve.open(os.path.join(TRAIN_DATA_PATH, 'train_clean.slv'))
        train_clean_speech = np.array(Train_Clean['training_clean_speech'])
        print('     Clean speech training data shape: %s' % str(train_clean_speech.shape))
        Train_Clean.close()

        # Load clean speech validation target data, which is also be used as speech component quality validation input. See the first term in (6)
        print('  >> Loading clean speech validation data... ')
        Valid_Clean = shelve.open(os.path.join(TRAIN_DATA_PATH, 'valid_clean.slv'))
        vali_clean_speech = np.array(Valid_Clean['validation_clean_speech'])
        print('     Clean speech validation data shape: %s' % str(vali_clean_speech.shape))
        Valid_Clean.close()

        # Generate training target for residual noise power, which should be zero. See the second term in (6)
        print('  >> Loading traning target for residual noise... ')
        train_residual_noise_power_target = np.zeros_like(train_pure_noise)
        print('     Training residual noise target data shape: %s' % str(train_residual_noise_power_target.shape))

        # Generate validation target for residual noise power, which should be zero. See the second term in (6)
        print('  >> Loading validation target for residual noise... ')
        vali_residual_noise_power_target = np.zeros_like(vali_pure_noise)
        print('     Validation residual noise target data shape: %s' % str(vali_residual_noise_power_target.shape))

        print('> Data loading finished . Reshaping...')

        ###############################################################################################################
        # 2.1 Data reshaping
        ###############################################################################################################

        vali_clean_speech = np.reshape(vali_clean_speech, (vali_clean_speech.shape[0], vali_clean_speech.shape[1], 1))
        print('     vali_clean_speech shape: %s' % str(vali_clean_speech.shape))
        train_clean_speech = np.reshape(train_clean_speech,
                                        (train_clean_speech.shape[0], train_clean_speech.shape[1], 1))
        print('     train_clean_speech shape: %s' % str(train_clean_speech.shape))

        vali_residual_noise_power_target = np.reshape(vali_residual_noise_power_target, (
            vali_residual_noise_power_target.shape[0], vali_residual_noise_power_target.shape[1], 1))
        print('     vali_residual_noise_power_target shape: %s' % str(vali_residual_noise_power_target.shape))
        train_residual_noise_power_target = np.reshape(train_residual_noise_power_target, (
            train_residual_noise_power_target.shape[0], train_residual_noise_power_target.shape[1], 1))
        print('     train_residual_noise_power_target shape: %s' % str(train_residual_noise_power_target.shape))

        train_pure_noise = np.reshape(train_pure_noise, (train_pure_noise.shape[0], train_pure_noise.shape[1], 1))
        print('     train_pure_noise shape: %s' % str(train_pure_noise.shape))
        vali_pure_noise = np.reshape(vali_pure_noise, (vali_pure_noise.shape[0], vali_pure_noise.shape[1], 1))
        print('     vali_pure_noise shape: %s' % str(vali_pure_noise.shape))
        toc = time.time()
        tictoc = toc - tic
        print(tictoc, 'Sekunden')

        ###############################################################################################################
        # 3 Model Definition
        ###############################################################################################################

        input_noisy = Input(shape=INPUT_SHAPE)
        input_noise_component = Input(shape=INPUT_SHAPE2)
        input_speech_component = Input(shape=INPUT_SHAPE2)

        # PRELU TUTORIAL:
        # https://www.machinecurve.com/index.php/2019/12/05/how-to-use-prelu-with-keras/
        # changed:
        # LeakyReLU(0.2)(c1)
        # to:
        # PReLU(alpha_initializer=initializers.Constant(value=0.2))(c1)

        c1 = Conv1D(n1, N_cnn, padding='same')(input_noisy)
        c1 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c1)
        c1 = Conv1D(n1, N_cnn, padding='same')(c1)
        c1 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c1)
        x = MaxPooling1D(2)(c1)

        c2 = Conv1D(n2, N_cnn, padding='same')(x)
        c2 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c2)
        c2 = Conv1D(n2, N_cnn, padding='same')(c2)
        c2 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c2)
        x = MaxPooling1D(2)(c2)

        c3 = Conv1D(n3, N_cnn, padding='same')(x)
        c3 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c3)
        x = UpSampling1D(2)(c3)

        c2_2 = Conv1D(n2, N_cnn, padding='same')(x)
        c2_2 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c2_2)
        c2_2 = Conv1D(n2, N_cnn, padding='same')(c2_2)
        c2_2 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c2_2)

        m1 = Add()([c2, c2_2])
        m1 = UpSampling1D(2)(m1)

        c1_2 = Conv1D(n1, N_cnn, padding='same')(m1)
        c1_2 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c1_2)
        c1_2 = Conv1D(n1, N_cnn, padding='same')(c1_2)
        c1_2 = PReLU(alpha_initializer=initializers.Constant(value=0.2))(c1_2)

        m2 = Add()([c1, c1_2])

        # Estimated mask from noisy input
        mask = Conv1D(1, N_cnn, padding='same', activation='sigmoid')(m2)

        # generate the filtered speech and noise component
        n_tilde = Multiply()([mask, input_noise_component])
        n_tilde_residual_power = Multiply()([mask, input_noise_component])
        s_tilde = Multiply()([mask, input_speech_component])

        model = Model(inputs=[input_noisy, input_noise_component, input_speech_component],
                      outputs=[n_tilde, n_tilde_residual_power, s_tilde])
        model.summary()

        ###############################################################################################################
        # 4 Training Settings
        ###############################################################################################################

        adam_wn = Adam(learning_rate=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)  # beta_1=0.9
        # adadelta = Adadelta(learning_rate=learning_rate, rho=0.95, epsilon=1e-7, name='Adadelta')

        # the loss_weights are corresponding the weighting factors in (6), with the order [\alpha, \beta, 1-\alpha-\beta]
        # model.compile(optimizer=adam_wn, loss=['mean_squared_error', my_loss, 'mean_squared_error'],
        # loss_weights=[0.1, 0.8, 0.1], metrics=['accuracy'])
        model.compile(optimizer=adam_wn, loss=['huber', my_loss, 'huber'], loss_weights=[0.1, 0.8, 0.1],
                      metrics=['mse'])  # [0.04, 0.9, 0.04]

        model.save_weights('resetmodel_3cl.h5')

        # Stop training after 10 epoches if the vali_loss not decreasing
        stop_str = cbs.EarlyStopping(monitor='val_loss', patience=10, verbose=1, mode='auto')

        # Reduce learning rate when stop improving lr = lr*factor
        reduce_LR = cbs.ReduceLROnPlateau(monitor='val_loss', factor=0.6, patience=10, verbose=1, mode='auto',
                                          min_delta=0.0001, cooldown=0, min_lr=0)

        # Save all models with every best weight configuration in different file locations
        best_weights = RESULTS_PATH + '/training_results_' + str(i + 1) + '/TIM_CNN_3CL' + '_{epoch:03d}' + '.h5'
        best_weights = os.path.normcase(best_weights)
        model_save = cbs.ModelCheckpoint(best_weights, monitor='val_loss', save_best_only=True,
                                         mode='auto')

        ###############################################################################################################
        # 5 Model Fitting
        ###############################################################################################################
        start = time.time()

        print(
            "> Training model " + "using Batch-size: " + str(batch_size) + ", Learning_rate: " + str(
                learning_rate) + "...")

        # Train the model
        hist = model.fit([train_input_noisy, train_pure_noise, train_clean_speech],
                         [train_residual_noise_power_target, train_pure_noise, train_clean_speech], epochs=nb_epochs,
                         verbose=1, batch_size=batch_size, shuffle=True, initial_epoch=0,
                         callbacks=[reduce_LR, model_save, stop_str],
                         validation_data=([vali_input_noisy, vali_pure_noise, vali_clean_speech],
                                          [vali_residual_noise_power_target, vali_pure_noise, vali_clean_speech])
                         )

        current_results_dir = os.path.join(RESULTS_PATH, 'training_results_' + str(i + 1))
        current_results_dir = os.path.normcase(current_results_dir)
        print(current_results_dir)
        Path(current_results_dir).mkdir(parents=True, exist_ok=True)  # create folder if not exists

        # Save data into a new shelve
        ff = shelve.open(os.path.join(os.path.dirname(current_results_dir), 'history.slv'))
        ff['train_loss'] = hist.history['loss']
        ff['val_loss'] = hist.history['val_loss']
        ff.close()

        # plot all loss values generated during training
        plot_learning_curves(hist.history['loss'], hist.history['val_loss'], current_results_dir)
        plt.clf()

        print("> Saving Completed, Time : ", time.time() - start, "s")
        print('> +++++++++++++++++++++++++++++++++++++++++++++++++++++ ')

        backend.clear_session()

    print("> All Trainings Completed, Duration : ", (time.time() - startAll) / 60 / 60, "h")
    print("> All Trainings Completed, Time : ", datetime.time())
    print('> +++++++++++++++++++++++++++++++++++++++++++++++++++++ ')


# this call is only for debugging. For executing the script use the main.py it calls multiple steps together
if __name__ == '__main__':
    train_main()
