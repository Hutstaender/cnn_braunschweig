import os
import shelve
import settings
from P04_Tools.plt_loss import plot_learning_curves
import matplotlib.pyplot as plt

RESULTS_PATH = settings.RESULTS_PATH

current_results_dir = os.path.join(RESULTS_PATH, 'training_results_' + str(2))
ff = shelve.open(os.path.join(os.path.dirname(current_results_dir), 'history.slv'))

plot_learning_curves(ff['train_loss'], ff['val_loss'], current_results_dir)
plt.close()
ff.close()