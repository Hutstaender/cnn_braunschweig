import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


def plot_learning_curves(loss, val_loss, path, pesq=[]):
    plt.plot(np.arange(len(loss)) + 0.5, loss, "b.-", label="Training loss")
    plt.plot(np.arange(len(val_loss)) + 1, val_loss, "r.-", label="Validation loss")
    if len(pesq) > 0:
        plt.plot(pesq, label="PESQ", secondary_y=True)
    plt.gca().xaxis.set_major_locator(mpl.ticker.MaxNLocator(integer=True))
    # plt.axis([1, 40, 0, 0.8])
    # plt.ylim([0, 0.05])
    plt.legend(fontsize=14)
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.grid(True)
    # hier muss man den Pfad angeben, damit die Grafik automatisch gespeichert wird
    plt.savefig(path + 'model_loss' + '.png')
