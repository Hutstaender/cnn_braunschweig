import glob
import os
import pathlib
import shelve

import matplotlib.pyplot as plt
import pandas as pd

import settings

RESULTS_PATH = settings.RESULTS_PATH
# this file is under construction and not for the general use!


# INFO_PATH = r"F:\Trainsets\Components-Loss-master_slv\training_results_16khz_28spk_2022-08-08_20validationdata_meansub_batch1024\final_audio_data_1\MeanScores_1_Epoch1.txt"
# x = pd.read_csv(INFO_PATH, sep="\t", header=None)

i = 2


def get_pesq_and_stoi_per_epoch_from_file(score_path=RESULTS_PATH, current_audio_set=i):
    # check for all MeanScore files
    history_files = [p for p in
                     glob.glob(
                         os.path.join(score_path, 'final_audio_data_' + str(current_audio_set), "**/MeanScores*.txt"),
                         recursive=True)]
    # print(history_files)

    df = pd.DataFrame()
    for i in range(0, len(history_files)):
        epoch = int(pathlib.PurePath(history_files[i]).name[18:-4])
        # print(pathlib.PurePath(history_files[0]).name[18:-4])

        csv_data = pd.read_csv(os.path.join(history_files[i]), sep="\t", header=None)
        pesq = float(csv_data[0][0][6:])
        stoi = float(csv_data[0][1][6:])

        new_line = pd.DataFrame({'epoch': epoch, 'pesq': pesq, 'stoi': stoi}, index=[i])

        df = pd.concat([df, new_line])

    df = df.sort_values(by='epoch')
    return df


pesq_stoi_df = get_pesq_and_stoi_per_epoch_from_file(RESULTS_PATH)

current_results_dir = os.path.join(RESULTS_PATH, 'training_results_' + str(i))
ff = shelve.open(os.path.join(os.path.dirname(current_results_dir), 'history.slv'))
# fig, ax =
d = {'train_loss': ff['train_loss'], 'val_loss': ff['val_loss']}
train_loss = pd.DataFrame(ff['train_loss'], columns=['train_loss'], index=list(range(1, len(ff['train_loss']) + 1)))
val_loss = pd.DataFrame(ff['val_loss'], columns=['val_loss'], index=list(range(1, len(ff['val_loss']) + 1)))
print(train_loss)
# df = pd.DataFrame(data=d)
# df['train_loss'].plot()
# df['val_loss'].plot(secondary_y=True, style="g")
# plt.show()
# plt.close()

ax = train_loss.plot(color="blue", marker='.')
ax = val_loss.plot(ax=ax, color="red", linestyle='dashed', marker='.')
pesq_stoi_df.plot(ax=ax, x='epoch', y='pesq', color="green", linestyle='dotted', marker='.', secondary_y=True)

# y labels
ax.set_ylabel("train_loss, val_loss")
ax.right_ax.set_ylabel("pesq")

# plt.show()
plt.savefig(os.path.join(os.path.dirname(current_results_dir), 'Loss_Pesq_training_results_' + str(i) + '.png'))
plt.close()

# plot_learning_curves(ff['train_loss'], ff['val_loss'], current_results_dir, ff['train_loss'])
ff.close()
