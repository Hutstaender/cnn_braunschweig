# CNN_Braunschweig

Current Master Project

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Hutstaender/cnn_braunschweig.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Hutstaender/cnn_braunschweig/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
CNN_TIM_SLV

## Description
This project involves a complete framework around the modified and updated CNN_Braunschweig. All Changes and extending information is listed in my master thesis.
Generally it uses .wav files, converts them to the frequency base, throws them into the CNN and reconstructs and scores everything in different ways.

## Installation
The project is set up in pycharm. For learning the first steps in Pycharm I used this tutorial: https://www.youtube.com/watch?v=dBMHuIWbF_k
To start the Project in Pycharm in the top bar go to vcs/git -> clone... -> enter the git url and clone the repository.
This repository only contains the framework information and no environmental files such as Python packages.
To install the Python packages open any file and look for red underlined imports. If any import isn't working look at the bottom bar in pycharm, click on Python Packages and search for the underlined package and install it.

## What you need
For using this framework you will need a full audio database. This file is not for generating noisy files it just uses the existing files.
You will need noisy and clean files for a train, validation and testset (6 different audio data folders with ".wav" files). All these files for this work are from the edinburg audio database. The needed pure noise files are calculated by subtracting clean speech from noisy speech.

## Usage
Every global variable is defined in the settings.py, if u first start on a new computer with different folders you will have to do some changes to the settings file.
For adding the information in the settings file, you will need .wav files for clean and noisy speech, as train validation and testset. (6 paths to the files)

This framework uses different stages. In the first stage the .wav files are combined to a single file in the data_preparation file


## Authors and acknowledgment
Tim Schneider - TH Aschaffenburg 2022

## License
Use only permitted upon request to the authors or in connection to Prof. Dr.-Ing. Mohammed Krini  (mohammed.krini@th-ab.de)