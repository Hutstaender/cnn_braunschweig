#############################################################
# Settings for the CNN Network:
#############################################################

# Data preparation paths ################
train_clean_path = r'F:\Trainsets\Components-Loss-master\audio_data\Edinburgh\01_train\clean'
train_noisy_path = r'F:\Trainsets\Components-Loss-master\audio_data\Edinburgh\01_train\noisy'
valid_clean_path = r'F:\Trainsets\Components-Loss-master\audio_data\Edinburgh\02_valid\clean'
valid_noisy_path = r'F:\Trainsets\Components-Loss-master\audio_data\Edinburgh\02_valid\noisy'
test_clean_path = r'F:\Trainsets\Components-Loss-master\audio_data\Edinburgh\03_test\clean'
test_noisy_path = r'F:\Trainsets\Components-Loss-master\audio_data\Edinburgh\03_test\noisy'

# Data preparation mode ################
# mode < 0  all modes enabled at the same time (recommended, otherwise some information may be lost)
# mode = 0  train_clean
# mode = 1  train_noisy(should be normalized)
# mode = 2  valid_clean
# mode = 3  valid_noisy(should be normalized)
# mode = 4  test_clean - may be removed
# mode = 5  test_noisy(should be normalized) - may be removed
# mode = 6  test_noisy_unnorm
# mode = 7  train_pure_noise
# mode = 8  valid_pure_noisy
# mode = 9  test_pure_noisy
DATA_PREPARATION_MODE = -1

# Generated train and test data folders ################
# These are the paths in which the files from data preparation are saved to
TRAIN_DATA_PATH = r'F:\Trainsets\Components-Loss-master_slv\audio_data\meanSub\training_data'
TEST_DATA_PATH = r'F:\Trainsets\Components-Loss-master_slv\audio_data\meanSub\test_data'

# Destination path to save the results to ################
# NOTE: Change this path everytime u want to test something about the network, otherwise the old results will be
# overwritten!
RESULTS_PATH = r'F:\Trainsets\Components-Loss-master_slv\training_results_16khz_28spk_2022-08-24_20validationdata_meansub_batch16384'

# FFT parameters ################
FFT = 512
FRAM_LENGTH = int(FFT / 2 + 1 + 3)  # + 1 because the center is shifted by 1, + 3 because of max-pooling

# The number of trainings that will be executed for the same parameters ################
# (ATTENTION: TF2 has a bug where all the graphics memory is allocated and not freed,
# you can only run as often as your memory has space. in case of Edinburgh data set 2-times)
NUMBER_OF_TRAININGS = 2

#############################################################
# Settings specifically for the LSTM Network extension:
#############################################################
# Generated train and test data folders for the lstm network
TRAIN_DATA_PATH_LSTM = r'F:\Trainsets\Components-Loss-master_slv\audio_data\meanSub\training_data_lstm'
TEST_DATA_PATH_LSTM = r'F:\Trainsets\Components-Loss-master_slv\audio_data\meanSub\test_data_lstm'
FFT_LSTM = 256
