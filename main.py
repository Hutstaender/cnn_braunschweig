
from P01_Data_Generator.Data_Preparation_Analysis_Filterbank import data_preparation_main
from P02_Training._MaskBased_CNN_Training_Multi import train_main
from P03_Evaluation.MaskBased_CNN_Test_Multi import test_main
from P03_Evaluation.Scoring_Objective_Analysis import score_main
from P03_Evaluation.Reconstruct_Synthesis_Filterbank import rec_main

###################################################
# 1 Main Program
###################################################
# execute this main program to run the following programs for every generated history file together (otherwise you
# need to start them one by one, by hand. And u need to outcomment the function call at the end of every file):
# -the data generation with analysis filterbank
# -a cnn training
# -a cnn test
# -reconstruction
# -pesq, stoi scoring


if __name__ == '__main__':
    # data_preparation_main()
    train_main()
    test_main()
    rec_main()
    score_main()
