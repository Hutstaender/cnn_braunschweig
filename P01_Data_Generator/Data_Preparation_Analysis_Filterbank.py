import glob
import os
import shelve
import time

import librosa
import numpy as np
from sklearn.preprocessing import StandardScaler

import settings

# INFO
#####################################################################################################
# mode < 0  all modes enabled at the same time
# mode = 0  train_clean
# mode = 1  train_noisy(should be normalized)
# mode = 2  valid_clean
# mode = 3  valid_noisy(should be normalized)
# mode = 4  test_clean - may be removed
# mode = 5  test_noisy(should be normalized) - may be removed
# mode = 6  test_noisy_unnorm
# mode = 7  train_pure_noise
# mode = 8  valid_pure_noisy
# mode = 9  test_pure_noisy

#######################################################################################################################
# 1 Preparation - Settings
#######################################################################################################################
train_clean_path = settings.train_clean_path
train_noisy_path = settings.train_noisy_path
valid_clean_path = settings.valid_clean_path
valid_noisy_path = settings.valid_noisy_path
test_clean_path = settings.test_clean_path
test_noisy_path = settings.test_noisy_path

mode = settings.DATA_PREPARATION_MODE

# this parameter changes the fft and the path where all results are saved to. Additionally it removes the 3 extra
# frequency bins during generation, because they are not needed by the LSTM network.
isLSTM = False
if isLSTM:
    FFT_SIZE = settings.FFT_LSTM
    TRAIN_DATA_PATH = settings.TRAIN_DATA_PATH_LSTM
    TEST_DATA_PATH = settings.TEST_DATA_PATH_LSTM
else:
    FFT_SIZE = settings.FFT
    TRAIN_DATA_PATH = settings.TRAIN_DATA_PATH
    TEST_DATA_PATH = settings.TEST_DATA_PATH

librosa_mag = True
cruse = True


#######################################################################################################################
# 2 Data Preparation
#######################################################################################################################
def data_preparation_main():
    tic = time.time()

    # check if paths exist, if not create them
    if not os.path.exists(TRAIN_DATA_PATH):
        os.makedirs(TRAIN_DATA_PATH)
    if not os.path.exists(TEST_DATA_PATH):
        os.makedirs(TEST_DATA_PATH)

    if mode == 0 or mode < 0:  # mode = 0  train_clean
        print("generating train_clean")
        mag, phase, file_name, file_length = getWaveDataFromFolder(train_clean_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TRAIN_DATA_PATH, "train_clean.slv")))
        g['training_clean_speech'] = mag
        g.close()
        del mag, phase, file_name, file_length
    if mode == 1 or mode < 0:  # mode = 1  train_noisy(should be normalized)
        print("generating train_noisy")
        mag, phase, file_name, file_length = getWaveDataFromFolder(train_noisy_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TRAIN_DATA_PATH, "train_noisy.slv")))
        # normalization
        scaler = StandardScaler(with_std=False)
        mag = scaler.fit_transform(mag)
        g['training_input_noisy'] = mag
        g.close()
        del mag, phase, file_name, file_length
    if mode == 2 or mode < 0:  # mode = 2  valid_clean
        print("generating valid_clean")
        mag, phase, file_name, file_length = getWaveDataFromFolder(valid_clean_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TRAIN_DATA_PATH, "valid_clean.slv")))
        g['validation_clean_speech'] = mag
        g.close()
        del mag, phase, file_name, file_length
    if mode == 3 or mode < 0:  # mode = 3  valid_noisy(should be normalized)
        print("generating valid_noisy")
        mag, phase, file_name, file_length = getWaveDataFromFolder(valid_noisy_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TRAIN_DATA_PATH, "valid_noisy.slv")))
        scaler = StandardScaler(with_std=False)
        mag = scaler.fit_transform(mag)
        g['validation_input_noisy'] = mag
        g.close()
        del mag, phase, file_name, file_length
    if mode == 4 or mode < 0:  # mode = 4  test_clean
        print("generating test_clean")
        mag, phase, file_name, file_length = getWaveDataFromFolder(test_clean_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TEST_DATA_PATH, "test_clean.slv")))
        g['spectrogram_test_clean'] = mag
        g.close()
        del mag, phase, file_name, file_length
    if mode == 5 or mode < 0:  # mode = 5  test_noisy(should be normalized)
        print("generating test_noisy")
        mag, phase, file_name, file_length = getWaveDataFromFolder(test_noisy_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TEST_DATA_PATH, "test_noisy.slv")))
        scaler = StandardScaler(with_std=False)
        mag = scaler.fit_transform(mag)
        g['spectrogram_test_noisy'] = mag
        g['phase_test_noisy'] = phase
        g.close()
        del mag, phase, file_name, file_length
    if mode == 6 or mode < 0:  # mode = 6  test_noisy_unnorm
        print("generating test_noisy_unnorm")
        mag, phase, file_name, file_length = getWaveDataFromFolder(test_noisy_path, cruse, librosa_mag)

        # g = shelve.open((os.path.join(TEST_DATA_PATH, "test_noisy_unnorm.slv")))
        # g['spectrogram_test_noisy_unnorm'] = mag
        # g['phase_test_noisy_unnorm'] = phase
        # g['file_names'] = file_name
        # g['file_length'] = file_length

        # this part generates the test_s_tilde_3cl file to exclude the neural network and test the analysis and synthesis
        # filterbank - for testing purposes only
        g = shelve.open((os.path.join(settings.RESULTS_PATH, "test_s_hat_3CL.slv")))
        g['test_s_hat'] = mag

        g.close()
        del mag, phase, file_name, file_length
    if mode == 7 or mode < 0:  # mode = 7  train_pure_noise
        print("generating train_pure_noise")
        mag, phase = getPureNoiseFromFolder(train_clean_path, train_noisy_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TRAIN_DATA_PATH, "train_pure_noise.slv")))
        g['training_pure_noise'] = mag
        g.close()
        del mag, phase
    if mode == 8 or mode < 0:  # mode = 8  valid_pure_noisy
        print("generating valid_pure_noisy")
        mag, phase = getPureNoiseFromFolder(valid_clean_path, valid_noisy_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TRAIN_DATA_PATH, "valid_pure_noisy.slv")))
        g['validation_pure_noise'] = mag
        g.close()
        del mag, phase
    if mode == 9 or mode < 0:  # mode = 9  test_pure_noisy
        print("generating test_pure_noisy")
        mag, phase = getPureNoiseFromFolder(test_clean_path, test_noisy_path, cruse, librosa_mag)

        g = shelve.open((os.path.join(TEST_DATA_PATH, "test_pure_noisy.slv")))
        g['spectrogram_test_pure_noise'] = mag
        g['test_pure_noise_phase'] = phase
        g.close()
        del mag, phase

    toc = time.time()
    tictoc = toc - tic
    print('Dauer: ', tictoc, ' Sekunden')


def getWaveDataFromFolder(data_path, cruse, librosa_mag):
    file_length = []
    file_name = []
    spec = []
    # read and save all Data_path
    pathlist1 = [p for p in glob.glob(data_path + "**/*.wav", recursive=False)]
    for count, line in enumerate(pathlist1):
        # read dada
        sig, rate = librosa.load(line, sr=16000)
        file_length.append(len(sig))
        file_name.append(os.path.basename(line))

        spec.append(sig)
    signal_data = np.concatenate(spec, axis=0)

    # length correction
    if cruse == True:
        signal_data = fftLengthFix(signal_data)

    if librosa_mag == True:
        mag, phase = calculateSpectrum(signal_data)

    return mag, phase, file_name, file_length


def getPureNoiseFromFolder(clean_path, noisy_path, cruse, librosa_mag):
    # search the path of wav datei
    pathlist1 = [p for p in glob.glob(clean_path + "**/*.wav", recursive=True)]
    pathlist2 = [p for p in glob.glob(noisy_path + "**/*.wav", recursive=True)]
    noise = []

    for i in range(len(pathlist1)):
        sig1, _ = librosa.load(pathlist1[i], sr=16000)
        sig2, _ = librosa.load(pathlist2[i], sr=16000)

        pure_noise = sig2 - sig1
        noise.append(pure_noise)
    pure_noise_ges = np.concatenate(noise, axis=0)

    if cruse == True:
        pure_noise_ges = fftLengthFix(pure_noise_ges)
    if librosa_mag == True:
        mag, phase = calculateSpectrum(pure_noise_ges)

    return mag, phase


def fftLengthFix(signal_data):
    return librosa.util.fix_length(signal_data, size=len(signal_data) + FFT_SIZE // 2)


def calculateSpectrum(signal_data):
    # calculate spectogram
    mag0, phase0 = librosa.magphase(
        librosa.stft(signal_data, window='hann', n_fft=FFT_SIZE, hop_length=int(FFT_SIZE / 2), win_length=FFT_SIZE,
                     center=True))
    phase0 = np.transpose(np.unwrap(np.angle(phase0)))
    phase = np.zeros((phase0.shape[0], phase0.shape[1] + 3))
    phase[:, :-3] = phase0
    mag0 = np.transpose(mag0)
    mag = np.zeros((mag0.shape[0], mag0.shape[1] + 3))
    mag[:, :-3] = mag0

    if isLSTM:
        return mag0, phase0
    else:
        return mag, phase


# this call is only for debugging. For executing the script use the main.py it calls multiple steps together
if __name__ == '__main__':
    data_preparation_main()
